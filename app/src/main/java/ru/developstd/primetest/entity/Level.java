package ru.developstd.primetest.entity;


public class Level {
    private int level;
    private String next_level;
    private int one_click;

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getNextLevel() {
        return next_level;
    }

    public void setNextLevel(String next_level) {
        this.next_level = next_level;
    }

    public int getOneClick() {
        return one_click;
    }

    public void setOneClick(int one_click) {
        this.one_click = one_click;
    }
}
