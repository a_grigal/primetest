package ru.developstd.primetest;

import java.util.LinkedHashMap;
import java.util.Map;

public class Data {

    private static Data mInstance = null;

    private Map map;

    private Data() {
        map = new LinkedHashMap<>();
    }

    public static Data getInstance() {
        if (mInstance == null) {
            mInstance = new Data();
        }
        return mInstance;
    }

    public Map getMap() {
        return this.map;
    }

    public void setMap(Map value) {
        map = value;
    }

}




