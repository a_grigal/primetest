package ru.developstd.primetest;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;
import retrofit.http.GET;
import ru.developstd.primetest.entity.Level;

public class RequestBuilder {
    // Endpoint
    private final static String ENDPOINT = "http://topsource.u0094765.cp.regruhosting.ru/prime/";

    private static Gson gson = null;
    private static RequestInterceptor requestInterceptor = null;

    public static LevelService build() {

        if (gson == null) {
            gson = getGson();
        }

        if (requestInterceptor == null) {
            requestInterceptor = new RequestInterceptor() {
                @Override
                public void intercept(RequestFacade request) {
                    request.addHeader("User-Agent", "app");
                    request.addHeader("Accept", "application/json");
                }
            };
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setRequestInterceptor(requestInterceptor)
                .setConverter(new GsonConverter(gson))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        return restAdapter.create(LevelService.class);

    }

    public static Gson getGson() {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
        return gson;
    }

    public interface LevelService {
        @GET("/json.php")
        void levels(Callback<List<Level>> cb);
    }
}
