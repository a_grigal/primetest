package ru.developstd.primetest.helper;


import android.content.Context;
import android.content.SharedPreferences;
import android.util.Pair;

import java.util.List;

import ru.developstd.primetest.Data;
import ru.developstd.primetest.entity.Level;

public class Utils {

    public static boolean isInteger(String str) {
        if (str == null) {
            return false;
        }
        int length = str.length();
        if (length == 0) {
            return false;
        }
        int i = 0;
        if (str.charAt(0) == '-') {
            if (length == 1) {
                return false;
            }
            i = 1;
        }
        for (; i < length; i++) {
            char c = str.charAt(i);
            if (c <= '/' || c >= ':') {
                return false;
            }
        }
        return true;
    }

    public static void fillMap(List<Level> result) {
        for (Level level : result) {
            Data.getInstance().getMap().put(level.getLevel(), new Pair<>(level.getNextLevel(), level.getOneClick()));
        }
    }

    public static void setScore(Context mContext, Integer mScore) {
        SharedPreferences prefs = mContext.getSharedPreferences("mScore", 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("mScore", mScore);
        editor.commit();
    }

    public static Integer getScore(Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences("mScore", 0);
        return prefs.getInt("mScore", 0);
    }

    public static void setCountClick(Context mContext, Integer mCountClick) {
        SharedPreferences prefs = mContext.getSharedPreferences("mCountClick", 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("mCountClick", mCountClick);
        editor.commit();
    }

    public static Integer getCountClick(Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences("mCountClick", 0);
        return prefs.getInt("mCountClick", 0);
    }

    public static void setCurrentLevel(Context mContext, Integer mCurrentLevel) {
        SharedPreferences prefs = mContext.getSharedPreferences("mCurrentLevel", 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("mCurrentLevel", mCurrentLevel);
        editor.commit();
    }

    public static Integer getCurrentLevel(Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences("mCurrentLevel", 0);
        return prefs.getInt("mCurrentLevel", 1);
    }

    public static void setBtnTitle(Context mContext, String mCurrentLevel) {
        SharedPreferences prefs = mContext.getSharedPreferences("mBtnTitle", 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("mBtnTitle", mCurrentLevel);
        editor.commit();
    }

    public static String getBtnTitle(Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences("mBtnTitle", 0);
        return prefs.getString("mBtnTitle", "+1");
    }

}
