package ru.developstd.primetest.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Pair;
import android.view.Display;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;
import java.util.Random;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import ru.developstd.primetest.Data;
import ru.developstd.primetest.R;
import ru.developstd.primetest.RequestBuilder;
import ru.developstd.primetest.entity.Level;
import ru.developstd.primetest.helper.Utils;

public class PrimeActivity extends AppCompatActivity {

    @InjectView(R.id.button)
    Button button;
    @InjectView(R.id.tv_score)
    TextView tvScore;
    @InjectView(R.id.tv_level)
    TextView tvLevel;
    @InjectView(R.id.tv_animate)
    TextView tvAnimate;
    @InjectView(R.id.progressBar)
    ProgressBar mProgressBar;

    private int score;
    private int countClick;
    private int currentLevel = 1;
    private String btnTitle = "+1";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prime);
        ButterKnife.inject(this);

        if (savedInstanceState == null) {
            loadData();

            score = Utils.getScore(this);
            countClick = Utils.getCountClick(this);
            currentLevel = Utils.getCurrentLevel(this);
            btnTitle = Utils.getBtnTitle(this);

        } else {
            score = (int) savedInstanceState.getSerializable("score");
            countClick = (int) savedInstanceState.getSerializable("countClick");
            currentLevel = (int) savedInstanceState.getSerializable("currentLevel");
            btnTitle = (String) savedInstanceState.getSerializable("button_title");
        }

        tvScore.setText(String.valueOf(score));
        tvLevel.setText(String.valueOf(currentLevel));
        button.setText(btnTitle);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("score", score);
        outState.putSerializable("countClick", countClick);
        outState.putSerializable("currentLevel", currentLevel);
        outState.putSerializable("button_title", btnTitle);
    }

    private void loadData() {
        Callback<List<Level>> cb = new Callback<List<Level>>() {
            @Override
            public void success(List<Level> result, Response response) {
                Utils.fillMap(result);
                mProgressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
            }
        };

        mProgressBar.setVisibility(View.VISIBLE);
        RequestBuilder.build().levels(cb);
    }

    public void clickButton(View view) {
        showAnimateText();
        Pair<String, Integer> levelPair = (Pair<String, Integer>) Data.getInstance().getMap().get(currentLevel);
        tvScore.setText(String.valueOf(score += levelPair.second));
        countClick += levelPair.second;
        button.setText(btnTitle);

        if (Data.getInstance().getMap().size() == currentLevel) {
            showDialog();
        } else {
            Pair<String, Integer> nextLevelPair = (Pair<String, Integer>) Data.getInstance().getMap().get(currentLevel + 1);
            if (Utils.isInteger(nextLevelPair.first)) {
                if (countClick > Integer.valueOf(nextLevelPair.first) - levelPair.second) {
                    currentLevel += 1;
                    tvLevel.setText(String.valueOf(currentLevel));
                    btnTitle = "+" + nextLevelPair.second;
                    button.setText(btnTitle);
                }
            }
        }
    }

    private void showAnimateText() {
        AlphaAnimation animation = new AlphaAnimation(1.0f, 0.0f);
        animation.setDuration(3000);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        Random rand = new Random();
        int txtWidth = tvAnimate.getWidth();
        int rndNumber = rand.nextInt(size.x - txtWidth * 2);

        tvAnimate.setX(rndNumber + txtWidth);
        tvAnimate.startAnimation(animation);

        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                tvAnimate.setText(button.getText());
                tvAnimate.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                tvAnimate.setVisibility(View.INVISIBLE);
            }
        });

    }

    private void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.game_over)
                .setCancelable(false)
                .setNegativeButton(R.string.start_game,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                reset();
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void reset() {
        score = 0;
        countClick = 0;
        currentLevel = 1;
        btnTitle = "+1";
        tvScore.setText("0");
        tvLevel.setText("1");
        button.setText("+1");
    }

    private void saveState() {
        Utils.setScore(this, score);
        Utils.setCountClick(this, countClick);
        Utils.setCurrentLevel(this, currentLevel);
        Utils.setBtnTitle(this, btnTitle);
    }

    public void clickButtonInfo(View view) {
        Intent intent = new Intent(this, InfoActivity.class);
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        saveState();
    }

}
