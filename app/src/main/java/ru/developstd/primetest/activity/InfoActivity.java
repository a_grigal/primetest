package ru.developstd.primetest.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Pair;
import android.view.LayoutInflater;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.Iterator;
import java.util.Map;

import ru.developstd.primetest.Data;
import ru.developstd.primetest.R;

public class InfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        TableLayout table = (TableLayout) InfoActivity.this.findViewById(R.id.table);

        final Map map = Data.getInstance().getMap();
        Iterator<Map.Entry<Integer, Pair>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Integer, Pair> entry = iterator.next();
            TableRow row = (TableRow) LayoutInflater.from(InfoActivity.this).inflate(R.layout.attrib_row, null);
            ((TextView) row.findViewById(R.id.attrib_level)).setText(entry.getKey().toString());
            ((TextView) row.findViewById(R.id.attrib_next_level)).setText(entry.getValue().first.toString());
            ((TextView) row.findViewById(R.id.one_click)).setText(entry.getValue().second.toString());
            table.addView(row);
        }
        table.requestLayout();

    }
}
